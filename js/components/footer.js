// TODO: Make this into a function

const currentYear = new Date().getFullYear();
const footerElements = document.getElementsByTagName('footer');

if (footerElements.length === 1) {
    // there should only ever be one of these
    const footer = footerElements[0];
    const margin = '9%';
    footer.innerText = `Created and maintained by ${Config.Owner}, ${currentYear}.`;
    footer.style.marginLeft = margin;
}
else if (Config.IsDebug){
    console.log("ERROR: Footer element missing from page.")
}