const getRandomNumber = (minimum, maximum) => {
    if (!minimum)
        minimum = 0;
    
    if(!maximum)
        maximum = 1;

    return Math.floor(Math.random() * (maximum - minimum) + minimum);
}