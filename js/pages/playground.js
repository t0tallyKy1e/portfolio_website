const characters = [
    // tank
    'D.Va',
    'Orisa',
    'Reinhardt',
    'Roadhog',
    'Sigma',
    'Winston',
    'Wrecking Ball',
    'Zarya',
    
    // damage
    'Ashe',
    'Bastion',
    'McCree',
    'Doomfist',
    'Echo',
    'Genji',
    'Hanzo',
    'Junkrat',
    'Mei',
    'Pharah',
    'Reaper',
    'Soldier 76',
    'Sombra',
    'Symmetra',
    'Torbjorn',
    'Tracer',
    'Widowmaker',

    // healing
    'Ana',
    'Baptiste',
    'Brigitte',
    'Lucio',
    'Mercy',
    'Moira',
    'Zenyatta'
];

const CHARACTER_TYPES = [
    'Any',
    'Tank',
    'Damage',
    'Healing',
];

const defaultStart = 0;
const defaultEnd = 31;

const tankStart = defaultStart;
const tankEnd = 7;
const damageStart = 8;
const damageEnd = 24;
const healingStart = 25;
const healingEnd = defaultEnd;

let selectedType = 0;

const init = () => {
    const element = document.getElementById('characterList');

    if (element) {
        CHARACTER_TYPES.forEach((type) => {
            var characterElement = document.createElement("option");
            characterElement.value = type;
            characterElement.innerText = type;
            element.appendChild(characterElement);
        })
    } else {
        console.error("init() error: character picker not found using id='characterList'.");
    }
}

const getStartingIndex = (type) => {
    let start = defaultStart;

    if (type === 'Tank') {
        start = tankStart;
    } else if (type === 'Damage') {
        start = damageStart;
    } else if (type === 'Healing') {
        start = healingStart;
    } else {
        start = defaultStart;
    }

    return start;
}

const getEndingIndex = (type) => {
    let end = defaultEnd;

    if (type === 'Tank') {
        end = tankEnd;
    } else if (type === 'Damage') {
        end = damageEnd;
    } else if (type === 'Healing') {
        end = healingEnd;
    } else {
        end = defaultEnd;
    }

    return end;
}

const getRandomOverwatchCharacterIndex = (type) => {
    const start = type ? getStartingIndex(type) : defaultStart;
    const end = type ? getEndingIndex(type) : defaultEnd;
    return getRandomNumber(start, end);
};

const getRandomOverwatchCharacter = () => {
    const element = document.getElementById('character');

    if (element) {
        element.innerText = characters[getRandomOverwatchCharacterIndex(selectedType)];
    }
    else {
        console.error('getRandomOverwatchCharacter() ERROR: Unable to find element with id="character".');
    }
};

const handleCharacterTypeChange = () => {
    const element = document.getElementById('characterList');

    if (element && element.value) {
        console.log(element.value);
        selectedType = element.value;
    } else {
        console.error('handleCharacterTypeChange() ERROR: selectedType not set - element has no value.');
    }
}

// TODO: Add a picker for different games
// TODO: Move globals to their own files
init();